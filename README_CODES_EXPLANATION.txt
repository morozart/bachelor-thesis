This README.txt file gives a brief explanation about codes and programs used during my bachelor thesis work

Context_restoration.py:
    In this file deep learning models (including upsampling layers) for context restoration method pretraining and training loop are implemented.

split_train_val_test.py:
    This file splits any dataset into training, validation and testing subsets.

create_batches_multi_input.py:
    This script creates batches that will be used for training process for multi input models.

create_batches_single_input.py:
    This script creates batches that will be used for training process for single input models.

datAugMultiInput.py:
    This file implements data augmentation strategy (including all geometric tranformations) for multi input models.

datAugSingleInput.py:
    This file implements data augmentation strategy (including all geometric tranformations) for single input models.

generate_Synthetic_data.py:
    This file has the function which creates synthetic dataset used in the degree of stenosis prediction experiment.

multiple_input_models.py:
    In this file deep learning architectures accepting two images are implemented and trained.

training_single_input_model.py:
    In this file two deep learning architectures (ResNet and DenseNet) are initialized and trained.

Statistics_computation.py:
    This script provides two functions for Peason and Spearman correlation coefficients computation.

SPSD_create.py:
    In this file creation of SPSD dataset is implemented.

Select_images_annotations.py:
    This file helps to select images for which segmentation and experts annotation are available

Choose_reliable_echogenicity.py:
    Script implements reliability estimation for echogenicity parameter as explained in 4.1.2 section (also another one version of this code was implemented and inserted in work to make it more simple
     and illustrative)

Choose_reliable_homogeneity.py:
  Script implements reliability estimation for homogeneity parameter as explained in 4.1.2 section (also another one version of this code was implemented and inserted in work to make it more simple
     and illustrative)

Compute_plaque_parameters.py:
    Script combines the information from ultrasound scan and corresponding segmentation and creates image where only plaque is located. Additionally computes mean plaque intensity and mean intensity
    standard deviation

MAIN_DATA.csv:
    csv table collects the necessary information for conducted experiments (Grouping images by patient, Left\rigt side, Long\trans orientation, normalized mean plaque intensity, normalized standard
    deviation, path to images etc.)