import numpy as np


def computeSpearman(true_labels, predicted_labels):
    """
    Function computes Spearman correlation coefficient between labels and model predictions
    :param true_labels: 
    :type ndarray:
    :param predicted_labels:
    :type ndarray:
    :return Spearman_corr:
    :type int:
    """
    predicted_labels = predicted_labels.flatten()
    true_labels_sorted = np.flip(np.sort(true_labels))
    predicted_labels_sorted = np.flip(np.sort(predicted_labels))

    summ = 0
    for i in range(len(true_labels_sorted)):
        rank_true_value = np.where(true_labels_sorted == true_labels[i])[0][0] + 1
        rank_results = np.where(predicted_labels_sorted == predicted_labels[i])[0][0] + 1

        summ += (rank_results - rank_true_value)**2
    Spearman_corr = 1 - (6 * summ / (len(true_labels) * (len(true_labels) ** 2 - 1)))
    return Spearman_corr


def computePearson(true_labels, predicted_labels):
    """
    Function computes Pearson correlation coefficient between labels and model predictions
    :param true_labels:
    :type ndarray:
    :param predicted_labels:
    :type ndarray:
    :return Pearson_corr:
    :type int:
    """
    predicted_labels = predicted_labels.flatten()

    sum_true = np.sum(true_labels, axis=0)
    sum_pred = np.sum(predicted_labels, axis=0)
    sum_sq_true = np.sum(np.square(true_labels), axis=0)
    sum_sq_pred = np.sum(np.square(predicted_labels), axis=0)
    dot_prod = np.dot(true_labels, predicted_labels)
    n = len(true_labels)
    Pearson_corr = (n * dot_prod - sum_true * sum_pred) / (np.sqrt(n * sum_sq_true - np.square(sum_true)) * np.sqrt(n * sum_sq_pred - np.square(sum_pred)) + 0.0000001)
    return Pearson_corr



