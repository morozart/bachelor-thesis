import numpy as np


def Compute_plaque_MEAN_ntensity_STD(US, SEG):
	"""
	Function combines information from ultrasound scan and corresponding segmentation,
	creates image where only an atherosclerotic plaque is located and additionally
	computes mean pixel intensity and mean standard deviation over the entire plaque
	:param US
	:type ndarray
	:param SEG
	:type ndarray
	:return: plaque_cropp, mean_intens, mean_intens_std
	:type ndarray, int, int
	"""
	assert US.shape == SEG.shape
	plaque_image = np.zeros((US.shape[0], US.shape[1], 3))

	plaque_image[:, :, :] = [0, 0, 0]
	pixels = []
	for x in range(SEG.shape[0]):
		for y in range(SEG.shape[1]):
			if np.array_equal(SEG[x, y], [0, 255, 0]):
				plaque_image[x, y] = US[x, y]
				pixels.append(np.sum(US[x, y])/3)
	plaque_cropp = plaque_image
	if len(pixels) == 0:
		return None, None, None
	else:
		return plaque_cropp, np.mean(np.array(pixels)), np.std(np.array(pixels))

