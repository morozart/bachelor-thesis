import csv
import numpy as np

file_cropped_data = open(".csv", "r")

csv_reader = csv.reader(file_cropped_data)
masks_cropped_data_intensity = []
intensity_list = []


for row in csv_reader:
	masks_cropped_data_intensity.append(row)
	intensity_list.append(float(row[-2]))


MAX_INTENS = 89.5
MIN_INTENS = min(intensity_list)

file_cropped_data.close()

id_set = set()

for row in masks_cropped_data_intensity:
	id_set.add(row[0])


id_intensity = dict()
id_path = dict()
id_echogenicity = dict()

for patient_id in id_set:
	left_intensity = list()
	right_intensity = list()
	left_path = list()
	right_path = list()
	left_echogenicity = None
	right_echogenicity = None
	left = False
	right = False
	for row in masks_cropped_data_intensity:
		if row[0] == patient_id:
			if row[2] == "left":
				left_intensity.append(float(row[-2]))
				left_path.append(row[1])
				left_echogenicity = float(row[-1])
				left = True
			elif row[2] == "right":
				right_intensity.append(float(row[-2]))
				right_path.append(row[1])
				right_echogenicity = float(row[-1])
				right = True
	if right:
		id_intensity[patient_id + "_r"] = right_intensity
		id_path[patient_id + "_r"] = right_path
		id_echogenicity[patient_id + "_r"] = right_echogenicity
	if left:
		id_intensity[patient_id + "_l"] = left_intensity
		id_path[patient_id + "_l"] = left_path
		id_echogenicity[patient_id + "_l"] = left_echogenicity


def Is_reliable(intensities, label):
	labels_intervals_dict = {"1.0": [0, 0.25], "2.0": [0.25, 0.5], "3.0": [0.5, 0.75], "4.0": [0.75, 1]}

	normalized_intensities = (np.array(intensities) - MIN_INTENS)/(MAX_INTENS - MIN_INTENS)
	validity = []
	img_found = False
	interval = labels_intervals_dict[str(label)]

	for intens in normalized_intensities:
		if (intens > interval[0] and intens < interval[1]):
			validity.append(1)
			img_found = True
		else:
			validity.append(0)
	if not img_found:
		min_dist = float("inf")

		index_found = -1
		for i in range(len(normalized_intensities)):

			if np.abs(normalized_intensities[i] - interval[0]) < min_dist:
				min_dist = np.abs(normalized_intensities[i] - interval[0])
				index_found = i
			if np.abs(normalized_intensities[i] - interval[1]) < min_dist:
				min_dist = np.abs(normalized_intensities[i] - interval[1])
				index_found = i
		validity[index_found] = 1
	return validity, normalized_intensities


csv_to_write = []

for key in id_intensity.keys():
	label = id_echogenicity[key]
	intensities = id_intensity[key]

	validity, normalized_intens = Is_reliable(intensities, label)
	paths = id_path[key]
	assert len(paths) == len(intensities) and len(intensities) == len(validity)
	for i in range(len(intensities)):
		csv_to_write.append([key[:-2], paths[i], key[-1], intensities[i], normalized_intens[i], label , validity[i]])

file_write_data = open("Data_echogenicity.csv", "w")

csvwrite = csv.writer(file_write_data)
csvwrite.writerow(["ID", "PATH", "SIDE", "INTENSITY", "NORMALIZED INTENSITY", "LABEL", "ECHOGENICITY RELIABILITY"])
csvwrite.writerows(csv_to_write)























