import random
import cv2
import numpy as np
import albumentations as A
import copy


def flipY(image):
    """
    Function implements image mirroring (flip) over Y axis
    :param image:
    :type ndarray:
    :return: image:
    :type ndarray:
    """
    middleX = round(image.shape[1] / 2)
    image_temp = np.zeros((image.shape[0], image.shape[1], image.shape[2]))

    for i in range(middleX):
        image_temp[:, i, :] = image[:, i, :]
        image[:,  i, :] = image[:, image.shape[1] - i - 1, :]
        image[:, image.shape[1] - i - 1, :] = image_temp[:, i, :]
    return image


def flipX(image):
    """
   Function implements image mirroring (flip) over X axis
   :param image:
   :type ndarray:
   :return: image:
   :type ndarray:
   """
    middleY = round(image.shape[0] / 2)
    image_temp = np.zeros((image.shape[0], image.shape[1], image.shape[2]))

    for i in range(middleY):
        image_temp[i, :, :] = image[i, :, :]
        image[i, :, :] = image[image.shape[1] - i - 1,:, :]
        image[image.shape[1] - i - 1, :, :] = image_temp[i, :, :]
    return image


def shiftX(image, shift):
    """
    Function implements image shifting in X axis
    Parameter shift represents the shift range
    :param image:
    :type ndarray
    :param shift:
    :type int
    :return: image:
    :type ndarray:
    """
    if shift > 0:
        image_temp = image[:, image.shape[1] - shift: image.shape[1], :]
        image[:, shift :, :] = image[:, :image.shape[1] - shift, :]
        image[:, :shift, :] = image_temp
    else:
        image_temp = image[:, : -shift, :]
        image[:, : image.shape[1] + shift, :] = image[:, -shift:, :]
        image[:, image.shape[0] + shift:, :] = image_temp
    return image


def shiftY(image, shift):
    """
    Function implements image shifting in Y axis
    Parameter shift represents the shift range
    :param image:
    :type ndarray
    :param shift:
    :type int
    :return: image:
    :type ndarray:
    """
    if shift > 0:
        image_temp = image[image.shape[0] - shift: image.shape[0], :, :]
        image[shift :, :, :] = image[:image.shape[0] - shift, :, :]
        image[:shift, :, :] = image_temp
    else:
        image_temp = image[: -shift, :, :]
        image[: image.shape[0] + shift, :, :] = image[-shift:, :, :]
        image[image.shape[0] + shift:, :, :] = image_temp
    return image


def rotate(image, angle):
    """
    Function implements image rotation
    :param image:
    :type ndarray
    :param angle:
    :type int
    :return: rotated_image
    :type ndarray
    """
    RotMatrix = cv2.getRotationMatrix2D((image.shape[0]//2, image.shape[1]//2), angle = angle, scale=1.0)
    rotated_image = cv2.warpAffine(image, RotMatrix, (image.shape[0], image.shape[1]))
    return rotated_image


def image_cropping(image, coords):
    """
    Function implements image crop transformation
    coords parameter is a 4 dimensional list
    :param image
    :type ndarray
    :return: cropped_image
    :type ndarray
    """
    x1, y1, x2, y2 = coords[0], coords[1], coords[2], coords[3]
    cropped_image = image[y1:y2, x1:x2, :]
    cropped_image = cv2.resize(cropped_image, (256, 256), interpolation=cv2.INTER_AREA)
    return cropped_image


def identical_function(image):
    return image


def multi_input_model_augmentation(image1, image2):
    """
    Function implements data augmentation strategy for two input images simultaneously.
    From 1 to 4 different geometric transformations are applied gradually
    The same transformations with equal parameters are applied on both images
    :param image1:
    :type ndarray
    :param image2:
    :type ndarray
    :return: augmented_image1:
    :type ndarray
    :return: augmented_image2:
    :type ndarray
    """
    augmentation_functions = {"rotation": rotate, "flipX": flipX, "flipY": flipY, "shiftX": shiftX, "shiftY": shiftY, "cropping": image_cropping, "identity": identical_function}

    N_aug_used = random.randint(1, 4)

    list_augs = list(augmentation_functions.keys())
    augs_indexis = random.sample(range(0, len(list_augs)), N_aug_used) # from 1 to 4 transformations are randomly chosen

    augmented_image1 = copy.deepcopy(image1)
    augmented_image2 = copy.deepcopy(image2)

    for index in augs_indexis:
        if list_augs[index] == "rotation":
            angle = 360 * random.random()
            augmented_image1 = rotate(augmented_image1, angle)
            augmented_image2 = rotate(augmented_image2, angle)
        elif list_augs[index] == "cropping":
            x1 = random.randint(0, 5)
            y1 = random.randint(0, 5)
            x2 = random.randint(251, 256)
            y2 = random.randint(251, 256)
            coords = [x1, y1, x2, y2]
            augmented_image1 = image_cropping(augmented_image1, coords)
            augmented_image2 = image_cropping(augmented_image2, coords)
        elif list_augs[index] == "shiftX":
            shift = random.randint(-4, 4)
            augmented_image1 = shiftX(augmented_image1, shift)
            augmented_image2 = shiftX(augmented_image2, shift)
        elif list_augs[index] == "shiftY":
            shift = random.randint(-4, 4)
            augmented_image1 = shiftY(augmented_image1, shift)
            augmented_image2 = shiftY(augmented_image2, shift)
        elif list_augs[index] == "flipX":
            augmented_image1 = flipX(augmented_image1)
            augmented_image2 = flipX(augmented_image2)
        elif list_augs[index] == "flipY":
            augmented_image1 = flipY(augmented_image1)
            augmented_image2 = flipY(augmented_image2)

    return augmented_image1, augmented_image2


