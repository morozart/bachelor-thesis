import cv2
import numpy as np
import torch
from numpy import asarray
from numpy import savetxt
import matplotlib.pyplot as plt
import random
from datAugSingleInput import *
import copy
import time
from torchvision import models
from Statistics_computation import *
PATH_PROJECT = "/home.stud/morozart/"


def save_history(MODEL_NAME, pretraining, augmentation = None, train_loss_history = None, val_loss_history = None, Spearman_history = None, Pearson_history = None):
    """
    Function stores training loss, validation loss, Spearman rank corr. coeff, Pearson rank corr. coeff histories obtained during the training.
    :param model_name:
    :type string:
    :param train_loss_history:
    :type ndarray:
    :param val_loss_history:
    :type ndarray:
    :param Spearman_history:
    :type ndarray:
    :param Pearson_history:
    :type ndarray:
    :return: None
    :type None:
    """
    savetxt("training_loss_history_{}_{}_pretraining_{}_augmentation.csv".format(MODEL_NAME, pretraining, augmentation), train_loss_history, delimiter=",")
    savetxt("validation_loss_history_{}_{}_pretraining_{}_augmentation.csv".format(MODEL_NAME, pretraining, augmentation), val_loss_history, delimiter=",")
    savetxt("Spearman_coef_history_{}_{}_pretraining_{}_augmentation.csv".format(MODEL_NAME, pretraining,augmentation), Spearman_history, delimiter=",")
    savetxt("Pearson_coef_history_{}_{}_pretraining_{}_augmentation.csv".format(MODEL_NAME, pretraining, augmentation), Pearson_history, delimiter=",")


def test_model(model, criterion = None, dataloaders = None, optimizer = None):
    """
    Function implements testing phase for CNN, computes loss, Spearman and Pearson corr. coeffs on testing data
    :param model, criterion = None, dataloaders = None, optimizer = None, concat_images = False
    :return None:
    """
    model.eval()
    all_outs = np.array([])
    all_labels = np.array([])
    total_loss = 0
    with torch.no_grad():
        for index, batch in enumerate(dataloaders["test"]):
            inputs = torch.from_numpy(batch["data"])/255 # normalize input image

            labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2  # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)

            optimizer.zero_grad()

            outputs = model(inputs.float().to(DEVICE))
            all_labels = np.append(all_labels, labels.cpu().numpy())
            all_outs = np.append(all_outs, outputs.cpu().detach().numpy())
            loss = criterion(outputs, labels.float())
            total_loss += loss.item()

            torch.cuda.empty_cache()
    PearsonTest = computePearson(all_labels, all_outs)
    SpearmanTest = computeSpearman(all_labels, all_outs)

    print("Pearson correlation: ", PearsonTest, "Spearman correlation: ", SpearmanTest)


def train_model(model, model_name, dataloaders, criterion, optimizer, num_epochs=100):
    """
    Function implements the main training process for models with two input images
    :param model, model_name, dataloaders, criterion, optimizer, num_epochs:
    :return None:
    """
    since = time.time()

    training_loss_history = []
    val_loss_history = []
    Pearson_history = []
    Spearman_history = []

    best_correlation = float('-inf')
    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs))
        print('-' * 30)

        for phase in ['train', 'val']:
            epoch_loss = 0.0
            if phase == 'train':
                model.train()
                with torch.enable_grad():
                    for index, batch in enumerate(dataloaders[phase]):
                        inputs = torch.from_numpy(batch["data"])/255  # normalize input img
                        labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2 # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)

                        inputs_after_augmentation = copy.deepcopy(inputs)
                        if DATA_AUGMENTATION:
                            for i in range(len(inputs)):
                                apply_augment = random.random()
                                if apply_augment > 0.4: # 60 % chance of augmentation
                                    inputs_after_augmentation[i] = torch.from_numpy(single_input_model_augmentation(inputs[i].numpy()))

                        optimizer.zero_grad()
                        inputs_after_augmentation = inputs_after_augmentation.permute(0, 3, 1, 2).to(DEVICE)
                        outputs = model(inputs_after_augmentation.float())

                        loss = criterion(outputs, labels.float())
                        loss.backward()
                        optimizer.step()

                        epoch_loss += loss.item()
                        torch.cuda.empty_cache()
                training_loss_history.append(epoch_loss)
                print('{} Loss: {:.4f}'.format(phase, epoch_loss))

            elif phase == "val":
                model.eval()
                whole_outs = np.array([])
                whole_labels = np.array([])
                with torch.no_grad():
                    for index, batch in enumerate(dataloaders[phase]):
                        optimizer.zero_grad()
                        inputs = torch.from_numpy(batch["data"])/255  # normalize input img
                        labels = torch.from_numpy(batch["labels"]).to(DEVICE)/2 # normalize label (for Homogeneity /2, for Echogenicity /4, for Degree of stenosis /100)

                        inputs = inputs.permute(0, 3, 1, 2)
                        outputs = model(inputs.float().to(DEVICE))

                        whole_labels = np.append(whole_labels, labels.cpu().numpy())
                        whole_outs = np.append(whole_outs, outputs.cpu().numpy())
                        loss = criterion(outputs, labels.float())

                        epoch_loss += loss.item()
                        torch.cuda.empty_cache()

                print("Outputs ", whole_outs)
                print("Labels  ", whole_labels)
                val_loss_history.append(epoch_loss)
                Spearman_corr_coef = computeSpearman(true_labels=whole_labels, predicted_labels=whole_outs)
                Pearson_corr_coef = computePearson(true_labels=whole_labels, predicted_labels=whole_outs)
                Pearson_history.append(Pearson_corr_coef)
                Spearman_history.append(Spearman_corr_coef)

                if Pearson_corr_coef > best_correlation: # save model weights according to the best Pearson correlation value
                    best_correlation = Pearson_corr_coef
                    print("Saving best model weights!")
                    torch.save(model.state_dict(), PATH_PROJECT + "{}_{}_pretraining_{}_augmentation_raw_homogeneity.pt".format(model_name, PRETRAINING, AUGMENTATION))
                print('{} Loss: {:.4f}, Spearman {}, Pearson {}'.format(phase, epoch_loss, Spearman_corr_coef, Pearson_corr_coef))

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    train_loss_history = asarray([training_loss_history])
    val_loss_history = asarray([val_loss_history])
    Spearman_history = asarray([Spearman_history])
    Pearson_history = asarray([Pearson_history])

    save_history(MODEL_NAME, pretraining=PRETRAINING, train_loss_history = train_loss_history, augmentation= AUGMENTATION, val_loss_history = val_loss_history,
                 Spearman_history = Spearman_history, Pearson_history = Pearson_history)


def set_grad_model_parameters(model_set_grad):
    """
      Function sets require gradient as True for all parameters of a model
      :param model:
      :type class 'torch.nn.modules.container.Sequential
      :return None:
      :type None:
    """
    for param in model_set_grad.parameters():
        param.requires_grad = True


def init_model_ResNet(pretrained=False):
    """
      Function creates model ResNet and changes fully connected layer to a more suitable for regression purpose
      :param pretrained:
      :type Bool:
      :return model:
      :type class 'torch.nn.modules.container.Sequential:
    """
    model = models.resnet34(pretrained=pretrained)
    set_grad_model_parameters(model)
    regression_model = torch.nn.Sequential(*(list(model.children())[:-2]))
    regression_model.fc = torch.nn.Sequential(torch.nn.Flatten(),
                                              torch.nn.Linear(32768 , 2048, bias=True),
                                              torch.nn.ReLU(),
                                              torch.nn.Linear(2048, 1024, bias=True),
                                              torch.nn.ReLU(),
                                              torch.nn.Linear(1024, 1, bias=True)) # replace FC layer to regression layer

    set_grad_model_parameters(regression_model)
    if pretrained == False:
        for lay in regression_model.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform(lay.weight)
    return regression_model


def init_model_DenseNet(pretrained=False):
    """
      Function creates model DenseNet and changes fully connected layer to a more suitable for regression purpose
      :param pretrained:
      :type Bool:
      :return model:
      :type class 'torch.nn.modules.container.Sequential:
    """
    model = models.densenet161(pretrained=pretrained)
    regression_model = torch.nn.Sequential(*(list(model.children())[:-1]))
    regression_model.classifier = torch.nn.Sequential(torch.nn.Flatten(),
                                                      torch.nn.Linear(141312, 2048, bias=True),
                                                      torch.nn.ReLU(),
                                                      torch.nn.Linear(2048, 1024, bias=True),
                                                      torch.nn.ReLU(),
                                                      torch.nn.Linear(1024, 1, bias=True)
                                                      )
    if pretrained == False:
        for lay in regression_model.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform(lay.weight)
    return regression_model


#----------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":

    DEVICE_NUMBER = 0
    if torch.cuda.is_available():
        print("CUDA IS AVAILABLE, DEVICE NUMBER {}".format(DEVICE_NUMBER))
        DEVICE = torch.device(DEVICE_NUMBER)
    else:
        print("NO CUDA IS AVAILABLE")
        DEVICE = torch.device("cpu")

    DATA_AUGMENTATION = True
    CTX_REST_PRETRAINED_WEIGHTS = True
    MODEL_RES_NET = False

    EPOCHS_NUMBER = 50
    BATCH_SIZE = 1

    data_loader = create_batches_single_input.BATCH_LOADER()
    #val_set = data_loader["val"]

    if MODEL_RES_NET:
        Learning_rate = 5 * 10**(-6)
        Weight_decay = 0.00005
        model = init_model_ResNet(pretrained=False)
        MODEL_NAME = "ResNet34"
    else:
        Learning_rate = 10**(-5)
        Weight_decay = 0.00005
        model = init_model_DenseNet(pretrained=False)
        MODEL_NAME = "DenseNet161"

    if CTX_REST_PRETRAINED_WEIGHTS:
        print("CONTEXT RESTORATION PRETRAINING WEIGHTS FOR MODEL {} ARE LOADED".format(MODEL_NAME))
        PRETRAINING = "CONTEXT_RESTORATION"
        model.load_state_dict(torch.load(PATH_PROJECT + "DenseNet161_weights_CONTEXT_RESTORATION_ULTRASOUND.pt".format(MODEL_NAME)))

    if torch.cuda.is_available():
        model = model.to(DEVICE)

    loss_function = torch.nn.MSELoss(reduction="mean")
    optimizer = torch.optim.Adam(model.parameters(), lr = Learning_rate, weight_decay=Weight_decay)

    print("START TRAINING MODEL {} WITH SIMPLE DATA AUGMENTATION".format(MODEL_NAME))
    train_model(model, model_name = MODEL_NAME, dataloaders=data_loader, optimizer=optimizer, num_epochs=EPOCHS_NUMBER, criterion=loss_function)

    test_model(model, criterion=loss_function, dataloaders=data_loader, optimizer = optimizer)


