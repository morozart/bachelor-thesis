
import random
import cv2
import numpy as np
import albumentations as A
import copy


def identical_function(image):
    return image


def flipY(image):
    """
   Function implements image mirroring (flip) over Y axis
   :param image:
   :type ndarray:
   :return: image:
   :type ndarray:
   """
    middleX = round(image.shape[1] / 2)
    image_temp = np.zeros((image.shape[0], image.shape[1], image.shape[2]))

    for i in range(middleX):
        image_temp[:, i, :] = image[:, i, :]
        image[:,  i, :] = image[:, image.shape[1] - i - 1, :]
        image[:, image.shape[1] - i - 1, :] = image_temp[:, i, :]
    return image


def flipX(image):
    """
  Function implements image mirroring (flip) over X axis
  :param image:
  :type ndarray:
  :return: image:
  :type ndarray:
  """
    middleY = round(image.shape[0] / 2)
    image_temp = np.zeros((image.shape[0], image.shape[1], image.shape[2]))

    for i in range(middleY):
        image_temp[i, :, :] = image[i, :, :]
        image[i, :, :] = image[image.shape[1] - i - 1,:, :]
        image[image.shape[1] - i - 1, :, :] = image_temp[i, :, :]
    return image


def shiftX(image):
    """
    Function implements image shifting in X axis
    :param image:
    :type ndarray
    :return: image:
    :type ndarray:
    """
    shift = random.randint(-4, 4)

    if shift > 0:
        image_temp = image[:, image.shape[1] - shift: image.shape[1], :]
        image[:, shift :, :] = image[:, :image.shape[1] - shift, :]
        image[:, :shift, :] = image_temp
    else:
        image_temp = image[:, : -shift, :]
        image[:, : image.shape[1] + shift, :] = image[:, -shift:, :]
        image[:, image.shape[0] + shift:, :] = image_temp
    return image


def shiftY(image):
    """
    Function implements image shifting in Y axis
    :param image:
    :type ndarray
    :return: image:
    :type ndarray:
    """
    shift = random.randint(-4, 4)

    if shift > 0:
        image_temp = image[image.shape[0] - shift: image.shape[0], :, :]
        image[shift :, :, :] = image[:image.shape[0] - shift, :, :]
        image[:shift, :, :] = image_temp
    else:
        image_temp = image[: -shift, :, :]
        image[: image.shape[0] + shift, :, :] = image[-shift:, :, :]
        image[image.shape[0] + shift:, :, :] = image_temp
    return image


def rotate(image):
    """
    Function implements image rotation
    :param image:
    :type ndarray
    :param angle:
    :type int
    :return: rotated_image
    :type ndarray
    """
    angle = 360 * random.random()

    RotMatrix = cv2.getRotationMatrix2D((image.shape[0]//2, image.shape[1]//2), angle = angle, scale=1.0)
    rotated_image = cv2.warpAffine(image, RotMatrix, (image.shape[0], image.shape[1]))
    return rotated_image


def image_cropping(image):
    """
    Function implements image crop transformation
    :param image
    :type ndarray
    :return: cropped_image
    :type ndarray
    """
    x1 = random.randint(0, 5)
    y1 = random.randint(0, 5)
    x2 = random.randint(250, 255)
    y2 = random.randint(250, 255)

    cropped_image = image[y1:y2, x1:x2, :]
    cropped_image = cv2.resize(cropped_image, (256, 256), interpolation=cv2.INTER_AREA)
    return cropped_image


def Speckle_noise(image):
    """
    Function implements adding speckle noise to imagee
    :param image:
    :type ndarray
    :return: sp_noise:
    :type ndarray
    """
    coefficient = 0.5 * random.random()
    sp_noise = image + image * coefficient * np.random.normal(0, 1, image.shape)
    return sp_noise


def Grid_distortion(image):
    """
    Function implements grid distortion transformation applied on input image
    :param image:
    :type ndarray
    :return: grid_dist_img:
    :type ndarray
    """
    transform = A.Compose(
        [A.GridDistortion(num_steps=10, distort_limit=0.25, interpolation=1, border_mode=0, always_apply = True, p=1),
         ])
    grid_dist_img = transform(image=image)['image']
    return grid_dist_img


def Elastic_transform(image):
    """
    Function implements elastic transformation applied on input image
    :param image:
    :type ndarray
    :return: el_trans_img:
    :type ndarray
    """
    transform = A.Compose(
        [A.ElasticTransform(alpha=10, sigma=50, alpha_affine=15, interpolation=1, border_mode=0, always_apply=True, p=1)
         ])
    el_trans_img = transform(image=image)['image']
    return el_trans_img


def single_input_model_augmentation(image):
    """
    Function implements data augmentation strategy
    From 1 to 4 different geometric transformations are applied gradually
    :param image:
    :type ndarray:
    :return augmented_image:
    :type ndarray
    """
    augmentation_functions = {"rotation": rotate, "flipX": flipX, "flipY": flipY, "shiftX": shiftX, "shiftY": shiftY, "cropping": image_cropping, "identity": identical_function, "speckle": Speckle_noise,
                              "elastic": Elastic_transform, "grid": Grid_distortion}

    N_aug_used = random.randint(1, 4)

    list_augs = list(augmentation_functions.keys())
    augs_indexis = random.sample(range(0, len(list_augs)), N_aug_used)
    augmented_image = copy.deepcopy(image)

    for index in augs_indexis:
        augmented_image = augmentation_functions[list_augs[index]](augmented_image)

    return augmented_image


