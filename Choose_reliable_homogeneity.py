import csv

import numpy as np

MEAN = 20.521807842626522
STD = 9.320993518119707
file_all = open("FILE_ALL.csv", "r")

csvreader = csv.reader(file_all)
header = next(csvreader)
all_rows = []

for row in csvreader:
	all_rows.append(row)

id_set = set()

for row in all_rows:
	id_set.add(row[0])


id_intens_std = dict()
id_homogen = dict()
id_other = dict()

for id_ in id_set:
	left_other = list()
	right_other = list()
	left_std_intensisty = list()
	right_std_intensisty = list()
	left_homogen = None
	right_homogen = None
	ifleft = False
	ifright = False
	for row in all_rows:
		other = []
		if row[0] == id_ and len(row[6]) > 0:
			if row[2] == "left":
				other.append(row[3])
				other.append(row[4])
				other.append(row[8])
				other.append(row[7])
				other.append(row[9])
				left_other.append(other)
				left_std_intensisty.append(float(row[5]))
				left_homogen = float(row[6])
				ifleft = True
			elif row[2] == "right":
				other.append(row[3])
				other.append(row[4])
				other.append(row[8])
				other.append(row[7])
				other.append(row[9])
				right_other.append(other)
				right_std_intensisty.append(float(row[5]))
				right_homogen = float(row[6])
				ifright = True
	if ifright:
		id_intens_std[id_ + "_r"] = right_std_intensisty
		id_other[id_ + "_r"] = right_other
		id_homogen[id_ + "_r"] = right_homogen
	if ifleft:
		id_intens_std[id_ + "_l"] = left_std_intensisty
		id_other[id_ + "_l"] = left_other
		id_homogen[id_ + "_l"] = left_homogen


def homogen_validity(stds, label):
	normalized_stds = (np.array(stds) - MEAN)/ STD
	labels_intervals_dict = {"1.0": [-10, 0], "2.0": [0, 10]}

	validity = []
	img_found = False
	interval = labels_intervals_dict[str(label)]

	for std in normalized_stds:
		if (std > interval[0] and std < interval[1]):
			validity.append(1)
			img_found = True
		else:
			validity.append(0)

	if not img_found:
		min_dist = float("inf")

		index_found = -1
		for i in range(len(normalized_stds)):

			if np.abs(normalized_stds[i] - interval[0]) < min_dist:
				min_dist = np.abs(normalized_stds[i] - interval[0])
				index_found = i
			if np.abs(normalized_stds[i] - interval[1]) < min_dist:
				min_dist = np.abs(normalized_stds[i] - interval[1])
				index_found = i
		validity[index_found] = 1
	return validity, normalized_stds


csv_to_write = []

for key in id_intens_std.keys():
	label = id_homogen[key]
	stds = id_intens_std[key]

	validity, normalized_stds = homogen_validity(stds, label)
	other = id_other[key]
	assert len(other) == len(stds) and len(stds) == len(validity)
	for i in range(len(stds)):
		side = None
		if key[-1] == "l":
			side = "left"
		elif key[-1] == "r":
			side = "right"
		one_row = [key[:-2], side, stds[i], normalized_stds[i], label , validity[i]]
		for j in other[i]:
			one_row.append(j)
		csv_to_write.append(one_row)

file_write_data = open("HOMOGENICITY_VALIDITY.csv", "w")

csvwrite = csv.writer(file_write_data)
csvwrite.writerow(["ID", "SIDE", "INTENSITY STD", "NORMALIZED INTENSITY STD", "HOMOGENICITY LABEL", "HOMOGENICITY VALID", "NORMALIZED INTENSITY", "ECHOGENICITY LABEL", "ECHOGENICITY VALID",
                   "ORIGINAL IMAGE PATH", "MASK PATH"])
csvwrite.writerows(csv_to_write)


