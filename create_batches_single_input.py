import cv2
import csv
import numpy as np


TRAINING_SET_SIZE = 0.85


def reshape(image):
	max_h = 256
	max_w = 256
	reshaped_image = cv2.resize(image, (max_w,max_h), interpolation=cv2.INTER_AREA)

	return reshaped_image


def read_images():

	images_train = list()
	labels_train = list()
	images_val = list()
	labels_val = list()
	images_test = list()
	labels_test = list()

	file = open("RAW_HOMOGENEITY_PREDICTION_TRANS_VALID_DIVIDED.csv", "r")
	csvreader = csv.reader(file)

	all_rows = []
	header = next(csvreader)
	for row in csvreader:
		all_rows.append(row)

	for row in all_rows:

		image = cv2.imread(row[-2])
		reshaped_img = reshape(image)
		label = float(row[5])
		if row[-1] == "train":
			images_train.append(reshaped_img)
			labels_train.append(label)
		elif row[-1] == "val":
			images_val.append(reshaped_img)
			labels_val.append(label)
		elif row[-1] == "test":
			images_test.append(reshaped_img)
			labels_test.append(label)

	images_train = np.array(images_train)
	images_val = np.array(images_val)
	images_test= np.array(images_test)

	labels_train = np.array(labels_train)
	labels_val = np.array(labels_val)
	labels_test = np.array(labels_test)
	return images_train, images_val, images_test, labels_train, labels_val, labels_test


def BATCH_LOADER():
	"""
	Function creates a dictionary with keys "train", "val", "test" by grouping batches of data. Each key corresponds to a subset (for training, testing, validation)
	:return: batch_loader
	:type dict
	"""
	images_train, images_val, images_test, labels_train, labels_val, labels_test = read_images()

	batches_train = create_batches(images_train, labels_train)

	batches_val = create_batches(images_val, labels_val)

	batches_test = create_batches(images_test, labels_test)

	return {"train": batches_train, "val": batches_val, "test": batches_test}


def create_batches(imagesUS, labels):
	"""
		Function creates dictionaries with labels "data" and "label", where "data" represents a pair an image and label represents experts annotation
		:param imagesUS:
		:type ndarray
		:param labels:
		:type ndarray
		:return: batches
		:type dict
	"""
	batches = list()
	for i in range(len(labels)):
		batch = {"data": np.array([imagesUS[i]]), "labels": np.array(labels[i])}
		batches.append(batch)

	return batches
