import copy
import time
from numpy import asarray
import torch.nn as nn
import torch
import creates_batches_single_input
import matplotlib.pyplot as plt
import numpy as np
import cv2
from torchvision import models
import random
from path_to_data import PATH_PROJECT
from training_single_input_model import save_history


def plot_train_history_graph(train_history):
    """
    Function plots the training loss history after context restoration
    :param train_history:
    :type ndarray:
    :return None:
    """
    file = open("training_loss_history_{}_{}_pretraining_phase.csv".format(MODEL_NAME, train_history))
    array = np.loadtxt(file, delimiter=",")
    plt.xlabel("Epoch")
    plt.ylabel("Training loss")
    plt.plot(array)
    plt.show()


def reshape(images):
    """
    Function reshapes a batch of images into 256x256 size
    :param images:
    :type ndarray:
    :return reshaped:
    :type ndarray:
    """
    max_h = 256
    max_w = 256
    reshaped_images = np.zeros((len(images), max_h, max_w, images[0].shape[2]))
    for i in range(len(images)):
        reshaped_images[i] = cv2.resize(images[i], (max_h,max_w), interpolation=cv2.INTER_AREA)

    return reshaped_images


def is_intersect(x1, y1, x2, y2, size):
    """
    Function tests whether two squares intersect (returns False) or no.
    Points (x1, y1) and (x2, y2) are left top corners of squares.
    size means the length (in pixels) of a square side
    :param x1, x2, y1, y2, size:
    :type int, int, int, int, int:
    :return True/False:
    :type bool:
    """
    if x2 > x1 and x2 < x1 + size and y2 > y1 and y2 < y1 + size:
        return False
    elif x2 + size > x1 and x2 + size < x1 + size and y2 > y1 and y2 < y1 + size:
        return False
    elif x2 + size > x1 and x2 + size < x1 + size and y2 + size > y1 and y2 + size < y1 + size:
        return False
    elif x2 > x1 and x2 < x1 + size and y2 + size > y1 and y2 + size < y1 + size:
        return False
    return True


def ContextDisorder(image):
    """
    Function corrupts image by swapping 50 disjunct squares of shape 10x10
    :param image:
    :type ndarray:
    :return image:
    :type ndarray:
    """
    for i in range(50):
        side = 10
        h = image.shape[0]
        w = image.shape[1]
        x1 = random.randrange(0, w - side)
        y1 = random.randrange(0, h - side)
        x2, y2 = -1, -1
        is_done = False
        while not is_done:
            x2 = random.randrange(0, w - side)
            y2 = random.randrange(0, h - side)
            is_done = is_intersect(x1, y1, x2, y2, side)
        patch1 = image[x1:x1 + side, y1:y1 + side, :]
        image[x1:x1 + side, y1:y1 + side, :] = image[x2:x2 + side, y2:y2 + side, :]
        image[x2:x2 + side, y2:y2 + side, :] = patch1
    return image


def BatchDisorder(batch):
    """
   Function corrupts image calls ContextDisorder and corrupts a batch of images
   :param batch:
   :type ndarray:
   :return batch:
   :type ndarray:
   """
    for i in range(len(batch)):
        batch[i] = torch.from_numpy(ContextDisorder(batch[i]))
    return batch


def save_state_dictionary_DENSENET(model_params):
    """
      Function removes upsampling layers from model state dictionary,
      initializes fully connected layer for regression task and
      saves parameters for DenseNet161 model for future initialization
      :param model_params:
      :return batch:
      :type ndarray:
      """
    fully_connected_layers = ["classifier.0.weight",
                              "classifier.0.bias",
                              "classifier.1.weight",
                              "classifier.1.bias",
                              "classifier.2.weight",
                              "classifier.2.bias",
                              "classifier.3.weight",
                              "classifier.3.bias"
                              ]
    for layer_name in fully_connected_layers:
        model_params.pop(layer_name)
    model_params["classifier.1.weight"] = torch.from_numpy(0.01 * np.random.randn(2048, 141312))
    model_params["classifier.1.bias"] = torch.from_numpy(0.01 * np.random.randn(2048))
    model_params["classifier.4.weight"] = torch.from_numpy(0.01 * np.random.randn(1024, 2048))
    model_params["classifier.4.bias"] = torch.from_numpy(0.01 * np.random.randn(1024))
    model_params["classifier.7.weight"] = torch.from_numpy(0.01 * np.random.randn(1, 1024))
    model_params["classifier.7.bias"] = torch.from_numpy(0.01 * np.random.randn(1))

    torch.save(model_params, PATH_PROJECT + "DenseNet161_weights_CONTEXT_RESTORATION_ULTRASOUND.pt")


def save_state_dictionary_RESNET(model_params):
    """
      Function removes upsampling layers from model state dictionary,
      initializes fully connected layer for regression task and
      saves parameters for ResNet 34 model for future initialization
      :param model_params:
      :return None:
      :type None:
      """
    fully_connected_layers = ["fc.0.weight", "fc.0.bias", "fc.1.weight", "fc.1.bias", "fc.2.weight", "fc.2.bias", "fc.3.weight", "fc.3.bias"]
    for layer_name in fully_connected_layers:
        model_params.pop(layer_name)
    model_params["fc.1.weight"] = torch.from_numpy(0.01 * np.random.randn(2048, 32768))
    model_params["fc.1.bias"] = torch.from_numpy(0.01 * np.random.randn(2048))
    model_params["fc.4.weight"] = torch.from_numpy(0.01 * np.random.randn(1024, 2048))
    model_params["fc.4.bias"] = torch.from_numpy(0.01 * np.random.randn(1024))
    model_params["fc.7.weight"] = torch.from_numpy(0.01 * np.random.randn(1, 1024))
    model_params["fc.7.bias"] = torch.from_numpy(0.01 * np.random.randn(1))

    torch.save(model_params, PATH_PROJECT + "ResNet34_weights_CONTEXT_RESTORATION_{}_Trans.pt".format(DATASET))


def set_grad_model_parameters(model):
    """
      Function sets require gradient as True for all parameters of a model
      :param model:
      :type class 'torch.nn.modules.container.Sequential
      :return None:
      :type None:
    """
    for param in model.parameters():
        param.requires_grad = True


def init_model_DenseNet():
    """
      Function creates model DenseNet 161 and replaces fully connected layer to upsampling layer in purpose of context restoration
       :param None:
      :return model:
      :type class 'torch.nn.modules.container.Sequential:
    """
    model = models.densenet161()
    set_grad_model_parameters(model)
    model = nn.Sequential(*(list(model.children())[:-1]))
    model.classifier = nn.Sequential(nn.ConvTranspose2d(2208, 32, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), output_padding=(0, 0)),
                                                nn.ConvTranspose2d(32,16, kernel_size=(3,3), stride=(3,3), padding = (1,1), output_padding=(0,0)),
                                                nn.ConvTranspose2d(16, 8, kernel_size=(3,3), stride=(3,3), padding = (1,1), output_padding=(0,0)),
                                                nn.ConvTranspose2d(8,3, kernel_size=(3,3), stride=(2,2), padding = (0,0), output_padding=(1,1)),
                                                ) # replace FC layer to upsampling layer
    for lay in model.modules():
        if type(lay) in [nn.Conv2d, nn.ConvTranspose2d]:
            torch.nn.init.xavier_uniform(lay.weight)
    return model


def init_model_ResNet():
    """
      Function creates model ResNet and replaces fully connected layer to upsampling layer in purpose of context restoration
      :param None:
      :return model:
      :type class 'torch.nn.modules.container.Sequential:
    """
    model = models.resnet34()
    set_grad_model_parameters(model)
    new_model = nn.Sequential(*(list(model.children())[:-2]))
    new_model.fc = nn.Sequential(nn.ConvTranspose2d(512, 32, kernel_size=(3,3), stride=(2,2), padding=(1,1), output_padding=(0,0)),
                                 nn.ConvTranspose2d(32,16, kernel_size=(3,3), stride=(3,3), padding = (1,1), output_padding=(0,0)),
                                 nn.ConvTranspose2d(16, 8, kernel_size=(3,3), stride=(3,3), padding = (1,1), output_padding=(0,0)),
                                 nn.ConvTranspose2d(8,3, kernel_size=(3,3), stride=(2,2), padding = (0,0), output_padding=(1,1)),
                                 ) # replace FC layer to upsampling layer
    for lay in model.modules():
        if type(lay) in [nn.Conv2d, nn.ConvTranspose2d]:
            torch.nn.init.xavier_uniform(lay.weight)
    return new_model


def train_Context_restoration(training_generator, optimizer, loss_func, model, epochs, model_name):
    """
      Function implements the main training process for context restoration
      :param training_generator, optimizer, loss_func, model, epochs, model_name:
      :return None:
    """
    model.train()
    minimal_loss = float("inf")
    loss_list = np.array([])
    since = time.time()
    for epoch in range(0, epochs ):
        running_loss = 0.0

        torch.cuda.empty_cache()
        with torch.set_grad_enabled(True):
            for index, batch in enumerate(training_generator["train"]):
                inputs = batch["data"]/255 # image normalization
                true_input = torch.from_numpy(copy.deepcopy(inputs))
                disordered_inputs = BatchDisorder(inputs)  # image corruption
                disordered_inputs = torch.from_numpy(disordered_inputs).permute(0, 3, 1, 2).to(DEVICE)
                outputs = model(disordered_inputs.float())
                outputs_reshaped = outputs.requires_grad_().float()

                orig_image = true_input.permute(0, 3, 1, 2).float().to(DEVICE)

                loss = loss_func(outputs_reshaped, orig_image)
                running_loss += loss
                loss_list = np.append(loss_list, running_loss.detach().cpu().numpy())
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

        if running_loss < minimal_loss and epoch % 50 == 0:  # each 50 epochs save model parameters
            minimal_loss = running_loss
            if RESNET:
                save_state_dictionary_RESNET(model.state_dict())
            else:
                save_state_dictionary_DENSENET(model.state_dict())
            print("Model parameters are saved")
        torch.cuda.empty_cache()
        print("Epoch {}, loss {}".format(epoch, running_loss))
    print("---" * 15)
    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    save_history(MODEL_NAME=model_name, train_loss_history=asarray([loss_list]), pretraining="CONTEXT_RESTORATION", save_all=False)


#---------------------------------------------------------------------------------------------------------------------------------------------------------
RESNET = True

DEVICE_NUMBER = 1
if torch.cuda.is_available():
    print("CUDA IS AVAILABLE, DEVICE NUMBER {}".format(DEVICE_NUMBER) )
    DEVICE = torch.device(DEVICE_NUMBER)
else:
    print("NO CUDA IS AVAILABLE")
    DEVICE = torch.device("cpu")


EPOCHS_NUMBER = 1500
data_dict_segmentation = BATCH_LOADER()


if RESNET:
    MODEL_NAME = "ResNet34"
    model = init_model_ResNet()
    print(type(model))
else:
    model = init_model_DenseNet()
    MODEL_NAME = "DenseNet161"


if torch.cuda.is_available():
    model = model.to(DEVICE)


print("STARTING CONTEXT RESTORATION PRETRAINING MODEL {}".format(MODEL_NAME))

loss_function = torch.nn.MSELoss()
optimizer = torch.optim.Adam(params=model.parameters(), lr = 1.5 * (10)**(-4))
train_Context_restoration(training_generator=data_dict_segmentation, optimizer=optimizer, loss_func=loss_function, model = model, epochs=EPOCHS_NUMBER, model_name=MODEL_NAME)
plot_train_history_graph(train_history="CONTEXT_RESTORATION")
