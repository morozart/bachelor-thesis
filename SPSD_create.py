import os

import cv2
import csv
import glob



def reshape(image):
	"""
	Function reshapes image into 256x256 shape
	:param image:
	:type ndarray
	:return: reshaped_image
	:type ndarray
	"""
	max_h = 256
	max_w = 256
	reshaped_image = cv2.resize(image, (max_w,max_h), interpolation=cv2.INTER_AREA)

	return reshaped_image


def Choose_single_plaque(image):
	"""
	Function takes an image where only plaque segments are present
	and selects images where only single plaque segment is represented.
	Returns reshaped plaque segment or None in case when several segments were detected
	:param image:
	:type ndarray:
	:return: reshaped_plaque or None
	:type ndarray / NoneType
	"""
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	edged = cv2.Canny(gray, 30, 200)

	contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

	if len(contours) == 1:
		x, y, w, h = cv2.boundingRect(contours[0])
		image_cropped = image[y + 1:y + h, x + 1: x + w]
		reshaped_plaque = reshape(image_cropped)
		return reshaped_plaque
	else:
		return None

