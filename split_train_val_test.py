import csv
import random
import numpy as np

file = open("File.csv", "r")


def generate_indexes(length):
	"""
	Function randomly selects and divides data points into training,
	validation and testing subsets
	:param length:
	:return train_index,
	:type list
	:return val_index,
	:type list
	:return test_index,
	:type list
	"""
	available_index = np.array(range(length))
	train_rand_index = random.sample(range(0, len(available_index)), round(0.85 * length))
	available_index = np.delete(available_index, train_rand_index)
	random.shuffle(available_index)
	train_rand_index = np.array(train_rand_index)
	val_rand_index = available_index[:int(len(available_index)/2)]
	test_rand_index = available_index[int(len(available_index)/2):]
	return sorted(train_rand_index), sorted(val_rand_index), sorted(test_rand_index)


csvreader = csv.reader(file)
header = next(csvreader)
all_rows = []

for row in csvreader:
	all_rows.append(row)

train_index, val_index, test_index = generate_indexes(len(all_rows))

for i in range(len(all_rows)):
	if i in train_index:
		all_rows[i].append("train")
	elif i in val_index:
		all_rows[i].append("val")
	elif i in test_index:
		all_rows[i].append("test")


file_to_write = open("FileTrainValTestSubsets.csv", "w")

csvwriter = csv.writer(file_to_write)
csvwriter.writerows(all_rows)

