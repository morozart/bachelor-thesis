import cv2
import csv
import numpy as np
import random
import matplotlib.pyplot as plt

TRAINING_SET_SIZE = 0.85


def reshape(image):
	max_h = 256
	max_w = 256
	reshaped_image = cv2.resize(image, (max_w,max_h), interpolation=cv2.INTER_AREA)

	return reshaped_image


def read_images():
	"""
	Function loads training, validation and testing subsets,
	Each subset has a number of  pairs ultrasound image - segmentation
	:return loaded pairs ultrasound image - segmentation and corresponding label for all three subsets:
	:type 9 ndarrays
	"""
	images_US_train = list()
	images_SEGM_train = list()
	labels_train = list()
	images_US_val = list()
	images_SEGM_val = list()
	labels_val = list()
	images_US_test = list()
	images_SEGM_test = list()
	labels_test = list()


	file = open("FileTrainValTestSubsets.csv", "r")
	csvreader = csv.reader(file)

	all_rows = []
	header = next(csvreader)
	for row in csvreader:
		all_rows.append(row)

	for row in all_rows:
		image_US = reshape(cv2.imread(row[-3]))
		image_SEGM = reshape(cv2.imread(row[-2]))
		label = float(row[5])
		if row[-1] == "train":
			images_US_train.append(image_US)
			images_SEGM_train.append(image_SEGM)
			labels_train.append(label)
		elif row[-1] == "val":
			images_US_val.append(image_US)
			images_SEGM_val.append(image_SEGM)
			labels_val.append(label)
		elif row[-1] == "test":
			images_US_test.append(image_US)
			images_SEGM_test.append(image_SEGM)
			labels_test.append(label)


	images_US_train = np.array(images_US_train)
	images_SEGM_train = np.array(images_SEGM_train)
	labels_train = np.array(labels_train)
	images_US_val = np.array(images_US_val)
	images_SEGM_val = np.array(images_SEGM_val)
	labels_val = np.array(labels_val)
	images_US_test = np.array(images_US_test)
	images_SEGM_test = np.array(images_SEGM_test)
	labels_test = np.array(labels_test)

	return images_US_train, images_SEGM_train, labels_train, images_US_val, images_SEGM_val, labels_val, images_US_test, images_SEGM_test, labels_test


def BATCH_LOADER():
	"""
	Function creates a dictionary with keys "train", "val", "test" by grouping batches of data. Each key corresponds to a subset (for training, testing, validation)
	:return: batch_loader
	:type dict
	"""
	images_US_train, images_SEGM_train, labels_train, images_US_val, images_SEGM_val, labels_val, images_US_test, images_SEGM_test, labels_test = read_images()

	batches_train = create_batches(images_SEGM_train, images_US_train, labels_train)

	batches_val = create_batches(images_SEGM_val, images_US_val, labels_val)

	batches_test = create_batches(images_SEGM_test, images_US_test, labels_test)
	batch_loader = {"train": batches_train, "val": batches_val, "test": batches_test}
	return batch_loader


def create_batches(imagesUS, imagesSEGM, labels):
	"""
	Function creates dictionaries with labels "data" and "label", where "data" represents a pair ultrasound - segmentation images and label represents experts annotation
	:param imagesUS:
	:type ndarray
	:param imagesSEGM:
	:type ndarray
	:param labels:
	:type ndarray
	:return: batches
	:type dict
	"""
	batches = list()
	for i in range(len(labels)):
		batch = {"data": (np.array([imagesUS[i]]), np.array([imagesSEGM[i]])), "labels": np.array(labels[i])}
		batches.append(batch)

	return batches
