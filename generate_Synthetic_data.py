import csv

import cv2
import random
from random import randint
import numpy as np
import os
import matplotlib.pyplot as plt
import albumentations as A
from datAugSingleInput import *
from numpy import savetxt


def ellipse(x, y ,center_x, center_y, a, b):
    """
    Function helps to test if is the point (x, y) inside ellipse with center (center_x, center_y ) and (a, b) are major and minor radius
    :param x:
    :type int
    :param y:
    :type int
    :param center_x:
    :type int
    :param center_y:
    :type int
    :param a:
    :type int
    :param b:
    :type int
    :return: True/False
    :type: Bool
    """
    return (y - center_y)**2 / b**2 + (x - center_x)**2 / a**2 < 1



def compute_proportion_green_blue(image):
    """
    Function computes a proportion of green pixels to the sum of green and blue pixels
    :param image:
    :return: proportion
    :type: float
    """
    N_blue = 0
    N_green = 0
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i, j, :][2] == 255:
                N_blue += 1
            elif image[i, j, :][1] == 255:
                N_green += 1
    proportion = N_green/(N_blue + N_green)
    return proportion


def rotate(image, angle):

    RotMatrix = cv2.getRotationMatrix2D((image.shape[0]//2, image.shape[1]//2), angle = angle, scale=1.0)
    rotated_image = cv2.warpAffine(image, RotMatrix, (image.shape[0], image.shape[1]))
    return rotated_image


def generate_synthetic_image():
    """
    Function creates an image for synthetic dataset by using ellipses and geometric transformations
    :return:  base_image, proportion_green_blue
    :type: ndarray, float
    """
    base_image = np.zeros((256, 256, 3))
    center_artery_y = randint(115, 140)
    center_artery_x = randint(115, 140)
    artery_diam_y = randint(90, 110)
    artery_diam_x = artery_diam_y + randint(-10, 10)
    plaque_diam_y = artery_diam_y - 10
    plaque_diam_x = artery_diam_x - 10
    lumen_diam_x = randint(int(plaque_diam_x * 0.3), int(plaque_diam_x * 0.8))
    while True:
        lumen_diam_y = randint(int(lumen_diam_x * 0.3), int(0.8 * lumen_diam_x))
        if lumen_diam_y != 0:
            break

    if lumen_diam_x > lumen_diam_y:
        lumen_center_x = center_artery_x
        lumen_center_y = center_artery_y + plaque_diam_y - lumen_diam_y
    else:
        lumen_center_y = center_artery_y
        lumen_center_x = center_artery_x + plaque_diam_x - lumen_diam_x
    is_green = random.random()
    for x in range(256):
        for y in range(256):
            if is_green > 0.15:
                if ellipse(x, y, center_artery_x, center_artery_y, artery_diam_x, artery_diam_y) and not ellipse(x, y, center_artery_x, center_artery_y, plaque_diam_x, plaque_diam_y) \
                        and not ellipse(x, y, lumen_center_x, lumen_center_y, lumen_diam_x, lumen_diam_y):
                    base_image[x, y, :] = [255, 0, 0] # fill walls with red color
                elif ellipse(x, y, center_artery_x, center_artery_y, plaque_diam_x, plaque_diam_y) and not ellipse(x, y, lumen_center_x, lumen_center_y, lumen_diam_x, lumen_diam_y):
                    base_image[x, y, :] = [0, 0, 255] # fill lumen with green color
                elif ellipse(x, y, lumen_center_x, lumen_center_y, lumen_diam_x, lumen_diam_y):
                    base_image[x, y, :] = [0, 255, 0] # fill plaque location with green color
            else: # 15 % do not have any plaque.
                if ellipse(x, y, center_artery_x, center_artery_y, artery_diam_x, artery_diam_y) and not ellipse(x, y, center_artery_x, center_artery_y, plaque_diam_x, plaque_diam_y) \
                        and not ellipse(x, y, lumen_center_x, lumen_center_y, lumen_diam_x, lumen_diam_y):
                    base_image[x, y, :] = [255, 0, 0]
                elif ellipse(x, y, center_artery_x, center_artery_y, plaque_diam_x, plaque_diam_y):
                    base_image[x, y, :] = [0, 0, 255]

    apply_rot = random.random()
    if apply_rot > 0.5:
        angle = 360 * random.random()
        base_image=rotate(base_image, angle)

    base_image = Grid_distortion(base_image) # apply grid distortion and elastic transform to make images more irregular and unsmooth
    base_image = Elastic_transform(base_image)

    # after grid distortion and elastic transform application some pixels lying close to margins between different colors change their color to some intermediate color. So I can restore the initial
    # color by knowing value of the dot product between actual pixel intensity and RGB color code.
    for x in range(256):
        for y in range(256):
            if not np.array_equal(base_image[x, y], [0., 255., 0.]) and not np.array_equal(base_image[x, y], [255., 0., 0.]) \
                and not np.array_equal(base_image[x, y], [0., 0., 255.]) and not np.array_equal(base_image[x, y], [0., 0., 0.]):
                if np.dot(base_image[x, y], [0., 255., 0.])/(np.linalg.norm(base_image[x, y]) *np.linalg.norm([0., 255., 0.]) ) >= 0.95:
                    base_image[x, y, :] = [0, 255, 0]
                elif np.dot(base_image[x, y], [0., 0., 255.])/(np.linalg.norm(base_image[x, y]) *np.linalg.norm([0., 0., 255.]) ) >= 0.95:
                    base_image[x, y, :] = [0, 0, 255]
                else:
                    base_image[x, y, :] = [255, 0, 0]

    plt.imshow(base_image)
    plt.show()
    proportion_green_blue = compute_proportion_green_blue(base_image)
    return base_image, proportion_green_blue
